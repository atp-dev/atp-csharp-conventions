ATP Code Conventions for C# Projects
====================================

Using the style prescribed by the configuration files in this repository helps to ensure ATP C# projects use a consistent and organised style.

Configuring a new project
-------------------------

 - Add this repository as a Git submodule with the path `$(SolutionDir)\.conventions`.
    - From the solution directory: `git submodule add https://bitbucket.org/atp-dev/atp-csharp-conventions.git .conventions`.
    - Preferably, mark the `.conventions` folder as hidden (but not its contents).
 - Import `AtpGroup.DotSettings` as solution team-shared Resharper settings (Resharper > Manage Options).
 - Add the generated `$(SolutionName).sln.DotSettings` file to `.gitignore`:
 
```
...
*.DotSettings
```

 - Configure the `.csproj` files for source projects:
 
```xml
<Project Sdk="Microsoft.NET.Sdk">
  ...
  <PropertyGroup>
    <DefineConstants>JETBRAINS_ANNOTATIONS</DefineConstants>
    <TreatWarningsAsErrors>true</TreatWarningsAsErrors>
    <CodeAnalysisRuleSet>$(SolutionDir)\.conventions\AtpGroup.ruleset</CodeAnalysisRuleSet>
  </PropertyGroup>
  <ItemGroup>
    <AdditionalFiles Include="$(SolutionDir)\.conventions\stylecop.json" Link="stylecop.json" />
  </ItemGroup>
  <ItemGroup>
    <PackageReference Include="JetBrains.Annotations" Version="11.0.0" />
    <PackageReference Include="StyleCop.Analyzers" Version="1.1.0-*" PrivateAssets="All" />
    ...
  </ItemGroup>
  ...
</Project>
```

 - Configure the `.csproj` files for test projects:
 
```xml
<Project Sdk="Microsoft.NET.Sdk">
  ...
  <PropertyGroup>
    <TreatWarningsAsErrors>true</TreatWarningsAsErrors>
    <CodeAnalysisRuleSet>$(SolutionDir)\.conventions\AtpGroup.Tests.ruleset</CodeAnalysisRuleSet>
  </PropertyGroup>
  <ItemGroup>
    <AdditionalFiles Include="$(SolutionDir)\.conventions\stylecop.json" Link="stylecop.json" />
  </ItemGroup>
  <ItemGroup>
    <PackageReference Include="JetBrains.Annotations" Version="11.0.0" />
    <PackageReference Include="xunit.analyzers" Version="0.7.0" />
    ...
  </ItemGroup>
  ...
</Project>
```

 - Commit these changes to the project.
    - `git commit -m "Add code conventions"`.
    
Updating conventions within a project
-------------------------------------

If ATP's code conventions have been updated and the conventions used by a project are stale, they can be updated using this process:

 - Update the `.conventions` submodule to the latest commit on master.
    - `git submodule update --remote .conventions`.
    
 - Re-import the potentially updated `AtpGroup.DotSettings` file as solution team-shared Resharper settings (Resharper > Manage Options).